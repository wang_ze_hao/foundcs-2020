# Foundations of Computer Science

[![Binder](https://mybinder.org/badge_logo.svg)][binder-all]

This repository will eventually contain the supervision work for my four FoundCS
supervisions (in `notebooks/sv{1,2,3,4}/questions.ipynb`). It also contains a
[rough OCaml style-guide][binder-style] for those looking to get more familiar
with OCaml.

## Doing the work

- Run one or [all][binder-all] of the notebooks in a server:

  - [Supervision 1][binder-sv1]
  - [Supervision 2][binder-sv2]
  
  <hr/>
 
  Be careful to save your work frequently to avoid losing it on the (temporary)
  server! If you have Docker installed, you can run the notebook server locally
  by doing `make run` and then view the notebooks by clicking on the final link.

- Read the work for your supervision and fill in the various `TODO` items.

- Save the resulting notebook and submit it to `me(at)craigfe(dot)io`. (Please
  send from your `@cam` address or include your CRSID in the email to simplify
  processing on my end.)

If you have any problems with the above process, don't hesitate to email me.

[binder-all]:   https://mybinder.org/v2/gl/CraigFe%2Ffoundcs-2020/main
[binder-sv1]:   https://mybinder.org/v2/gl/CraigFe%2Ffoundcs-2020/main?filepath=sv1%2Fquestions.ipynb
[binder-sv2]:   https://mybinder.org/v2/gl/CraigFe%2Ffoundcs-2020/main?filepath=sv2%2Fquestions.ipynb
[binder-style]: https://mybinder.org/v2/gl/CraigFe%2Ffoundcs-2020/main?filepath=style.ipynb
