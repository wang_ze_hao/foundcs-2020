{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Writing OCaml with style\n",
    "\n",
    "\n",
    "\n",
    "The Foundations of Computer Science course isn't fundamentally about OCaml; it's\n",
    "about introducing you to reasoning formally about programs. \n",
    "\n",
    "That said, you're going to end up writing quite a bit of OCaml code in\n",
    "supervisions, ticks and in your exams. It's worth spending some time to become\n",
    "familiar with OCaml in order to reach the stage where you're no longer \"learning\n",
    "OCaml\", but rather \"learning _using_ OCaml\" (as quickly as possible). This guide\n",
    "is about bootstrapping that process, so takes a different focus from the course\n",
    "lecture notes.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The components of OCaml programs\n",
    "\n",
    "OCaml has relatively few core syntax constructs. In this\n",
    "course, you'll probably only interact with the following four:\n",
    "\n",
    "- **structure items**. These are \"top-level definitions\" in an OCaml file or\n",
    "  sent to an OCaml REPL. In this course, you'll only need a few of these:\n",
    "  \n",
    "  ```ocaml\n",
    "  type t = t1              (* Defining types *)\n",
    "  let p1 (p2 ... pN) = e1  (* Defining values (which may be functions with parameters) *)\n",
    "  exception Foo            (* Defining exceptions *)\n",
    "  ```\n",
    "  \n",
    "  Executing an OCaml program consists of evaluating the structure items in\n",
    "  order. (Note that _functions_ are already values, and so there's no computation\n",
    "  associated with defining them.) The conventional structure of an OCaml program \n",
    "  is to have a sequence of type/function definitions, optionally followed by a \n",
    "  single `let () = ...` item that performs any side-effects (calling functions, \n",
    "  printing results, running tests etc.).\n",
    "  \n",
    "  For those who are more familiar with other\n",
    "  languages that have a magical \"main\" function called on startup (e.g. Java, C, and \n",
    "  to some extent Python), this idea of evaluating _all_ items in order on startup\n",
    "  can be surprising. (See \"[Where is `main`?](https://dev.realworldocaml.org/files-modules-and-programs.html#scrollNav-1-1)\")\n",
    " \n",
    "  > _Aside: you'll also see `val f : (* ... *)`, for specifying types of\n",
    "  > corresponding `let` definitions; this is technically a \"signature item\". In\n",
    "  > reality, there are several more types of structure item that form the OCaml\n",
    "  > \"[module\n",
    "  > system](https://dev.realworldocaml.org/files-modules-and-programs.html)\".\n",
    "  > These are important for using OCaml in the real world (and are most of what\n",
    "  > makes OCaml an interesting language), but aren't relevant to the teaching\n",
    "  > aims of the course._\n",
    " \n",
    "  <br>\n",
    "\n",
    "- **types**, `t`. These can appear in the body of type definitions and in type\n",
    "  constraints in expressions.\n",
    "\n",
    "  ```ocaml\n",
    "  unit, bool, int, string, char, etc.    (* Built-in abstract types *)\n",
    "  'a tree, (int * int), char list, etc.  (* Type operators: building types from types *)\n",
    "  'a, 'b, 'c, etc.                       (* Type variables. Read as \"alpha\", \"beta\", \"gamma\", etc. *)\n",
    "  \n",
    "  (* Algebraic data types *)\n",
    "  Branch of 'a * 'a tree * 'a | Leaf  (* Variant *)\n",
    "  { code : int; message : string }    (* Record *)\n",
    "  ```\n",
    "\n",
    "  > _Aside: algebraic types (variants and records) must be assigned a name with\n",
    "  > a `type <name> = ...` definition before use, and they can only be referred\n",
    "  > to via these names. Unlike, for instance, the tuple `int * int` which exists\n",
    "  > independently of any name assigned to it. Formally, we say that variants and\n",
    "  > records are the only \"nominal\" (name-bearing) types in OCaml, whereas the\n",
    "  > rest are \"structural\" types. This has some important implications for type\n",
    "  > inference and generating useful type errors, which we will not cover in this\n",
    "  > course._\n",
    "  \n",
    "<br>\n",
    " \n",
    "- **expressions**, `e`. These are the logical \"code\" of OCaml, appearing in the\n",
    "  body of top-level `let` bindings. Many of them contain nested sub-expressions\n",
    "  and patterns, allowing more complicated programs to be built from simpler\n",
    "  ones:\n",
    "  \n",
    "  ```ocaml\n",
    "  (* Constant values, e.g. *)\n",
    "  1  \"foo\"  ()  true  Leaf  (fun p -> e)\n",
    " \n",
    "  (* Tuples, lists, variants, records *) \n",
    "  (e1, e2)  [ e1; e2 ]  (Branch e)\n",
    " \n",
    "  e1 e2                   (* function [e1] applied to actual parameter [e2] *)\n",
    "  e1 |> e2                (* infix function [|>] applied to [e1] and [e2] *)\n",
    "  if e1 then e2 else e3   (* conditional *)\n",
    "  let p = e1 in e2        (* let-binding *)\n",
    "  e1; e2                  (* sequencing *)\n",
    "  (e : t)                 (* type constraint *)\n",
    "  ```\n",
    "  \n",
    "  **Note: the structure item `let p = e` is distinct from the expression `let p = e1 in e2`**. \n",
    "  The former is a structure item that defines a _top-level_ value that may be referenced by\n",
    "  later structure items; the latter is an _expression_ that introduces a local binding (and so \n",
    "  can only ever exist in the body of the former).\n",
    "  \n",
    "  The process of \"running\" an OCaml program consists of reducing expressions to\n",
    "  values in a pre-determined order. For instance, given expression `e = (let x = e1 in e2)`, we first reduce `e1` to a value `v`, then the value of `e` becomes\n",
    "  whatever `e2` reduces to given that `x` is bound to `v`. This idea of\n",
    "  \"computation\" as a process of gradually reducing expressions to values is\n",
    "  **very important**, not only for this course but for much of the rest of the\n",
    "  Tripos. (Don't worry if you don't understand this yet, we'll cover it in\n",
    "  supervisions.)\n",
    "  \n",
    "  Those of you who've done some programming before may be surprised to find that\n",
    "  there is no syntactic notion of a \"statement\" in OCaml, such as exists in\n",
    "  Python / JavaScript / C / Java etc. This follows directly from the fact that\n",
    "  variable bindings in OCaml are immutable and (almost) all control flow is\n",
    "  expressed via reduction of expressions (no `break`, `continue`, `return`,\n",
    "  etc.).\n",
    " \n",
    "<br>\n",
    " \n",
    "- **patterns**, `p`. Patterns are how variables get introduced in OCaml code:\n",
    "\n",
    "  ```ocaml\n",
    "  (* Constant patterns *) \n",
    "  1  \"foo\"  ()  true\n",
    "  \n",
    "  (* Variable names *)\n",
    "  x  y  _\n",
    "  \n",
    "  (* Tuples, lists, variants, records *)\n",
    "  (x, y, 3)  [ 1; 2; _ ]\n",
    "  ```\n",
    "  \n",
    "  These are hopefully fairly self-explanatory: patterns appear in `let`-bindings,\n",
    "  `match` cases, `fun` parameters etc., wherever data of some static structure\n",
    "  is _consumed_. We can use complex patterns to extract sub-components of complex\n",
    "  data:\n",
    "  \n",
    "  ```ocaml\n",
    "  let (a, b, _, d) = (1, 2, 3, 4) in\n",
    "  (* ... *)\n",
    "  ```\n",
    "  \n",
    "  (This feature is often called _\"destructuring\"_, especially for languages that\n",
    "  don't have a built-in notion of patterns and so include it as a tacked-on extra.) \n",
    "  If a pattern imposes some structure that is _not_ \n",
    "  implied by the type-system, this is detected by the compiler:\n",
    "  \n",
    "  ```ocaml\n",
    "  let head :: tail = [1; 2; 3] in\n",
    "  (* Warning 8: this pattern-matching is not exhaustive.\n",
    "   * Here is an example of a case that is not matched:\n",
    "   * []\n",
    "   *)\n",
    "  ```\n",
    "  \n",
    "\n",
    "<br>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Writing OCaml like you know what you're doing\n",
    "\n",
    "Here's a few tips for structuring your OCaml programs in a coherent manner."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Splitting a problem into steps\n",
    "\n",
    "It's often worthwhile to split up large function definitions into multiple sub-problems that can be solved individually. There are two ways to do this: defining multiple structure items, or defining inner values with `let`-bindings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "val utility : 'a -> 'b -> 'c = <fun>\n"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "val subproblem : 'a -> 'b = <fun>\n"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "val big_hard_problem : 'a -> 'b = <fun>\n"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "val big_hard_problem : 'a -> 'b = <fun>\n"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(* --- Multiple structure items -------------------------------- *)\n",
    "\n",
    "let utility x y = (* ... *) assert false\n",
    "let subproblem x =  (* ... *) assert false\n",
    "\n",
    "let big_hard_problem x =\n",
    "  utility 1 (subproblem x)\n",
    "\n",
    "(* --- Inner `let`-bindings ------------------------------------ *)\n",
    "\n",
    "let big_hard_problem =\n",
    "  let utility x y = (* ... *) assert false in\n",
    "  let subproblem x = (* ... *) assert false in\n",
    "  fun x ->\n",
    "    utility 1 (subproblem x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whichever of these you pick is up to you (but please – for the sake of my sanity – do pick one of them).\n",
    "\n",
    "A particularly common instance of this pattern is for recursive functions that need to either do some initial set-up or final transformation before returning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "val map : ('a -> 'b) -> 'c -> 'a list -> 'b list = <fun>\n"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "let map f xs =\n",
    "  (* [aux] doesn't need to take [f] as argument, since it never changes *)\n",
    "  let rec aux acc = function\n",
    "    | [] -> List.rev acc\n",
    "    | x :: xs -> aux (f x :: acc) xs\n",
    "  in\n",
    "  aux [] (* Need to pass initial accumulator, [] *)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sequential data transformations\n",
    "\n",
    "\n",
    "One of the consequences of the semantics of OCaml is that large programs tend to naturally run \"inside-out\": if we have a nested series of function applications, the innermost function is applied first, then the next innermost etc. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "21\n",
      "23\n"
     ]
    }
   ],
   "source": [
    "let () =\n",
    "  List.iter (Printf.printf \"%d\\n%!\") (      (* 5: finally, print the integers *)\n",
    "    List.map (( + ) 20) (                   (* 4: add 20 to each odd integer *)\n",
    "      List.filter (fun x -> x mod 2 = 1) (  (* 3: get the odd integers *)\n",
    "        List.map int_of_string (            (* 2: convert integers to strings *)\n",
    "          [ \"1\"; \"2\"; \"3\" ]))))             (* 1: initial data here *)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This becomes obnoxious to read fairly quickly. There are two ways to structure the code more cleanly:\n",
    "\n",
    "1. **name intermediate values with `let ... in ...`**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "21\n",
      "23\n"
     ]
    }
   ],
   "source": [
    "let () =\n",
    "  let xs = [ \"1\"; \"2\"; \"3\" ] in\n",
    "  let xs = List.map int_of_string xs in\n",
    "  let xs = List.filter (fun x -> x mod 2 = 1) xs in\n",
    "  let xs = List.map (( + ) 20) xs in\n",
    "  List.iter (Printf.printf \"%d\\n%!\") xs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. **use the pipe operator, `|>`**\n",
    "\n",
    "Solution (1) has the obvious problem that we need to come up with names for those intermediate results (and those names might not be enlightening). We can avoid this by using the pipe operator (`|>`), which takes a value on the left and a function to apply on the right:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "21\n",
      "23\n"
     ]
    }
   ],
   "source": [
    "let () =\n",
    "  [ \"1\"; \"2\"; \"3\" ]\n",
    "  |> List.map int_of_string\n",
    "  |> List.filter (fun x -> x mod 2 = 1)\n",
    "  |> List.map (( + ) 20)\n",
    "  |> List.iter (Printf.printf \"%d\\n%!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, it doesn't matter much which of these two options you pick, but please do pick one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Separating structure items and `;;`\n",
    "\n",
    "The OCaml parser doesn't need to be told when one structure item has finished and the next begins. This is unambigious purely from the grammar, even without considering whitespace or newlines. This means that it's possible to write some horrifically unreadable valid OCaml programs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "val a : int = 3\n"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "val b : int = 3\n",
       "val c : int = 9\n"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/plain": [
       "type foo = unit\n"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "let a = 1 + 2 let b = a and c = 9 type foo = unit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please put structure items on different lines, even though you don't have to. (In the real-world, we get around this maliciously permissive parser by using code-formatting tools like [OCamlformat](https://github.com/ocaml-ppx/ocamlformat). If you go to the effort of setting up an OCaml environment on your machine, I highly recommend using that too.)\n",
    "\n",
    "Even though it's not _necessary_ for the user to explicitly split up structure items, OCaml provides the ugly `;;` token for doing so. This is sometimes helpful for debugging syntax errors in your code, but is _never_ necessary in real-world OCaml. Its only real use is for the OCaml top-level environment, where it's necessary to explicitly end one structure item (in order to run it) _before_ starting the next one.\n",
    "\n",
    "In particular, don't confuse `;;` with the sequencing operator ` ( ; ) : unit -> 'a -> 'a` that is sometimes used in effectful code. As it happens, `( ; )` isn't necessary in OCaml programs either,\n",
    "\n",
    "```ocaml\n",
    "let p = e1; e2\n",
    "\n",
    "(* is equivalent to *)\n",
    "\n",
    "let p =\n",
    "  let () = e1 in\n",
    "  e2\n",
    "```\n",
    "\n",
    "so if you want to be on the safe-side, don't use either of them."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "OCaml ocaml.4.10.0",
   "language": "OCaml",
   "name": "ocaml-jupyter"
  },
  "language_info": {
   "codemirror_mode": "text/x-ocaml",
   "file_extension": ".ml",
   "mimetype": "text/x-ocaml",
   "name": "OCaml",
   "nbconverter_exporter": null,
   "pygments_lexer": "OCaml",
   "version": "4.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
